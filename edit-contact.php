<?php
include_once "includes/functions.inc.php";
if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $query = "SELECT * FROM contacts WHERE id = $id";
    $rows = db_select($query);
    if (count($rows) === 0) {
        die(var_dump("Cannot find resource having id: {$id}"));
        // instead of dd() u could redirect of 404 page
    }
    $row = db_select($query)[0];
} else if (isset($_POST['submit'])) {
    $id = sanitizeData($_POST['id']);

    // verify that id exists
    $query = "SELECT * FROM contacts WHERE id = $id";
    $rows = db_select($query);
    if (count($rows) === 0) {
        die(var_dump("Cannot find resource having id: {$id}"));
        // instead of dd() u could redirect of 404 page
    }
    $row = db_select($query)[0];

    $first_name = sanitizeData($_POST['first_name']);
    $last_name = sanitizeData($_POST['last_name']);
    $email = sanitizeData($_POST['email']);
    $birthdate = sanitizeData($_POST['birthdate']);
    $birthdate = date('Y-m-d', strtotime($birthdate));
    $telephone = sanitizeData($_POST['telephone']);
    $address = sanitizeData($_POST['address']);
    $image_name = strtolower($first_name . "-" . $last_name);
    $image_name = strtolower($first_name . "-" . $last_name);
    $image_path = "images/users/";
    $is_image_updated = false;

    if ($_FILES['pic']['name']) {
        $is_image_updated = true;
        $file_name = $_FILES['pic']['name'];
        $tmp_file_location = $_FILES['pic']['tmp_name'];

        $temp = explode(".", $file_name);
        $extension = strtolower(end($temp));
        $image_name .= '.' . $extension;
        $full_image_name_with_path = $image_path . $image_name;
        move_uploaded_file($tmp_file_location, $full_image_name_with_path);
    }

    /**
     * Either image is updated or name is updated or nothing is updated.
     * If only image is updated we can directly create or link the new img which we did,
     * if only name is updated , we need to rename file and update img_name in database
     * if both name and image is updated we need to link new file with new name, update img_name in database and remove old image file from device
     */

    $old_first_name = strtolower($row['first_name']);
    $old_last_name = strtolower($row['last_name']);

    $old_full_name = $old_first_name . " " . $old_last_name;
    $current_full_name = strtolower($first_name) . " " . strtolower($last_name);

    $old_image_name = $row['image_name'];
    $old_extension = end(explode(".", $old_image_name));
    if ($old_full_name === $current_full_name) {
        // no issues as new img is already overwritten
        // $image_name = $image_name . "." . $old_extension;
    } else {
        if ($is_image_updated) {
            unlink($image_path . $old_image_name);
        } else {
            $old_file = $image_path . $old_image_name;
            $image_name = $image_name . "." . $old_extension;

            $new_file = $image_path . $image_name;
            rename($old_file, $new_file);
        }
    }

    // update in db
    $query = "UPDATE contacts SET first_name = '${first_name}', last_name = '${last_name}', email = '${email}', birthdate = '${birthdate}', telephone = '${telephone}', address = '${address}', image_name ='${image_name}' WHERE id = ${id}";
    $result = db_query($query);
    if (!$result) {
        die(var_dump(db_error()));
    } else {
        header("Location: index.php?q=success&op=edit");
    }
} else {
    die('You cannot access this page!');
}
?>
<!DOCTYPE html>
<html>

<head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" />

    <!--Import Csutom CSS-->
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>Edit Contact</title>
</head>

<body>
    <!--NAVIGATION BAR-->
    <nav>
        <div class="nav-wrapper">
            <!-- Dropdown Structure -->
            <ul id="dropdown1" class="dropdown-content">
                <li><a href="#!">Profile</a></li>
                <li><a href="#!">Signout</a></li>
            </ul>
            <nav>
                <div class="nav-wrapper">
                    <a href="#!" class="brand-logo center">Contact Info</a>
                    <ul class="right hide-on-med-and-down">

                        <!-- Dropdown Trigger -->
                        <li><a class="dropdown-trigger" href="#!" data-target="dropdown1"><i class="material-icons right">more_vert</i></a></li>
                    </ul>
                </div>
            </nav>
            <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        </div>
    </nav>
    <!--/NAVIGATION BAR-->
    <div class="container">
        <div class="row mt50">
            <h2>Edit Contact</h2>
        </div>
        <!-- <div class="row">
            <div class="materialert">
                <i class="material-icons">check_circle</i> <span>Bienvenido, Linebeck</span>
                <button type="button" class="close-alert">×</button>
            </div>
            <div class="materialert info">
                <div class="material-icons">info_outline</div>
                Oh! What a beautiful alert :)
                <button type="button" class="close-alert">×</button>
            </div>
            <div class="materialert error">
                <div class="material-icons">error_outline</div>
                Oh! What a beautiful alert :)
                <button type="button" class="close-alert">×</button>
            </div>
            <div class="materialert success">
                <div class="material-icons">check</div>
                Oh! What a beautiful alert :)
                <button type="button" class="close-alert">×</button>
            </div>
            <div class="materialert warning">
                <div class="material-icons">warning</div>
                Oh! What a beautiful alert :)
                <button type="button" class="close-alert">×</button>
            </div>
        </div> -->
        <div class="row">
            <form class="col s12 formValidate" action="<?= $_SERVER['PHP_SELF']; ?>" id="edit-contact-form" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="id" id="contact_id" value="<?= $row['id']; ?>" readonly>
                <div class="row mb10">
                    <div class="input-field col s6">
                        <input id="first_name" name="first_name" type="text" class="validate" data-error=".first_name_error" value=<?= $row['first_name']; ?>>
                        <label for="first_name">First Name</label>
                        <div class="first_name_error "></div>
                    </div>
                    <div class="input-field col s6">
                        <input id="last_name" name="last_name" type="text" class="validate" data-error=".last_name_error" value=<?= $row['last_name']; ?>>
                        <label for="last_name">Last Name</label>
                        <div class="last_name_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s6">
                        <input id="email" name="email" type="email" class="validate" data-error=".email_error" value=<?= $row['email']; ?>>
                        <label for="email">Email</label>
                        <div class="email_error "></div>
                    </div>
                    <div class="input-field col s6">
                        <input id="birthdate" name="birthdate" type="text" class="datepicker" data-error=".birthday_error">
                        <label for="birthdate">Birthdate</label>
                        <div class="birthday_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s12">
                        <input id="telephone" name="telephone" type="tel" class="validate" data-error=".telephone_error" value=<?= $row['telephone']; ?>>
                        <label for="telephone">Telephone</label>
                        <div class="telephone_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="input-field col s12">
                        <textarea id="address" name="address" class="materialize-textarea" data-error=".address_error"><?= $row['address']; ?></textarea>
                        <label for="address">Addess</label>
                        <div class="address_error "></div>
                    </div>
                </div>
                <div class="row mb10">
                    <div class="col s2">
                        <img id="temp_pic" src="images/users/<?= $row['image_name']; ?>" alt="User" width="100%">
                    </div>
                    <div class="file-field input-field col s10">
                        <div class="btn">
                            <span>Image</span>
                            <input type="file" name="pic" id="pic" data-error=".pic_error">
                        </div>
                        <div class="file-path-wrapper">
                            <input class="file-path validate" type="text" placeholder="Upload Your Image">
                        </div>
                        <div class="pic_error "></div>
                    </div>
                </div>
                <button class="btn waves-effect waves-light right" type="submit" name="submit">Submit
                    <i class="material-icons right">send</i>
                </button>
            </form>
        </div>
    </div>
    <footer class="page-footer p0">
        <div class="footer-copyright ">
            <div class="container">
                <p class="center-align">© Keval Sanghvi</p>
            </div>
        </div>
    </footer>
    <!--JQuery Library-->
    <script src="js/jquery.min.js" type="text/javascript"></script>
    <!--JavaScript at end of body for optimized loading-->
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <!--JQuery Validation Plugin-->
    <script src="vendors/jquery-validation/validation.min.js" type="text/javascript"></script>
    <script src="vendors/jquery-validation/additional-methods.min.js" type="text/javascript"></script>
    <!--Include Page Level Scripts-->
    <script src="js/pages/edit-contact.js"></script>
    <!--Custom JS-->
    <script src="js/custom.js" type="text/javascript"></script>

    <script>
        $("#birthdate").datepicker({
            defaultDate: new Date('<?= $row['birthdate']; ?>'),
            setDefaultDate: true
        });
    </script>
</body>

</html>