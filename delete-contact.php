<?php
require("includes/functions.inc.php");

if (isset($_GET['id'])) {
    // Delete contact with this id
    $id = $_GET['id'];

    $rows = db_select("SELECT * FROM contacts WHERE id = $id");
    if ($rows === false) {
        $error = db_error();
        die(var_dump($error));
    }
    $image_name = $rows[0]['image_name'];
    unlink("images/users/$image_name");
    // Query to delete from DB
    $sql = "DELETE FROM contacts WHERE id = $id";
    $result = db_query($sql);

    if ($result) {
        header("Location: index.php?q=success&op=delete");
    } else {
        header("Location: index.php?q=error&op=delete");
    }
}
